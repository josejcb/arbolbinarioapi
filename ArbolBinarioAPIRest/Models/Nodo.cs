﻿namespace ArbolBinarioAPIRest.Models
{
    public class Nodo
    {
        public int numero;
        public Nodo derecho;
        public Nodo izquierdo;

        public Nodo(int valor)
        {
            numero = valor;
        }

        public Nodo Insertar(Nodo nodo, int dato)
        {
            if (nodo == null)
                nodo = new Nodo(dato);
            else if (nodo.numero < dato)
                nodo.derecho = Insertar(nodo.derecho, dato);
            else if (nodo.numero > dato)
                nodo.izquierdo = Insertar(nodo.izquierdo, dato);
            return nodo;
        }

        public int BuscarAncestro(Nodo actual, int nodoBusqueda, Nodo ancestro)
        {
            if (actual == null)
                return 0;

            if (actual.numero == nodoBusqueda)
            {
                return ancestro.numero;
            }
            else if (actual.numero < nodoBusqueda)
            {
                return BuscarAncestro(actual.derecho, nodoBusqueda, actual);
            }
            else if (actual.numero > nodoBusqueda)
            {
                return BuscarAncestro(actual.izquierdo, nodoBusqueda, actual);
            }else return 0;
        }
    }
}