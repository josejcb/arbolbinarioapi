﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ArbolBinarioAPIRest.Models;
using Microsoft.AspNetCore.Mvc;

namespace ArbolBinarioAPIRest.Controllers
{
    [Route("api/[controller]")]
    public class ArbolBinarioController : Controller
    {
        // POST api/values
        [HttpPost("ancestrocercano")]
        public int GetAncestroCercano(string nodos, int nodo1, int nodo2)
        {
            try
            {
                Arbol b = new Arbol();
                string[] entradaNodos;
                //Se separan las lineas ingresadas
                string[] entradaLineas = nodos.Split(new[] { "\r\n", "\r", "\n", Environment.NewLine }, StringSplitOptions.None);
                foreach (var linea in entradaLineas)
                {
                    entradaNodos = linea.Split(",");
                    //Se separan los nodos de las lineas
                    foreach (var nodo in entradaNodos)
                    {
                        //Se inserta nodo para armar el arbol
                        b.Insertar(Convert.ToInt32(nodo));
                    }
                }
                //Se busca el nodo ancestro comun mas cercano entre 2 nodos
                return b.Buscar(nodo1, nodo2);
            }
            catch { return 0; }
        }
    }
}
